Source: python-pbcore
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               dh-sequence-python3,
               dh-sequence-sphinxdoc <!nodoc>,
               python3-sphinx <!nodoc>,
               python3-all,
               python3-setuptools,
               python3-h5py,
               python3-numpy,
               python3-pysam,
               python3-pytest <!nocheck>,
               python3-uritools <!nocheck>,
               python3-pytest-runner <!nocheck>,
               python3-pytest-xdist <!nocheck>,
               python3-pytest-cov <!nocheck>,
               python3-biopython <!nocheck>,
               python3-xmlschema <!nocheck>,
               pylint,
               python3-coverage
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/med-team/python-pbcore
Vcs-Git: https://salsa.debian.org/med-team/python-pbcore.git
Homepage: https://github.com/PacificBiosciences/pbcore
Rules-Requires-Root: no

Package: python3-pbcore
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-biopython
Recommends: python3-h5py
Suggests: python-pbcore-doc (= ${source:Version})
Provides: python-pbcore
Description: Python 3 library for processing PacBio data files
 The pbcore package provides Python modules for processing Pacific Biosciences
 data files and building PacBio bioinformatics applications. These modules
 include tools to read/write PacBio data formats, sample data files for
 testing and debugging, base classes, and utilities for building bioinformatics
 applications.
 .
 This package is part of the SMRTAnalysis suite.
 .
 This is the Python 3 module.

Package: python-pbcore-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
         libjs-jquery,
         libjs-underscore
Description: Python library for processing PacBio data files (documentation)
 The pbcore package provides Python modules for processing Pacific Biosciences
 data files and building PacBio bioinformatics applications. These modules
 include tools to read/write PacBio data formats, sample data files for
 testing and debugging, base classes, and utilities for building bioinformatics
 applications.
 .
 pbcore is part of the SMRTAnalysis suite. This package provides the common
 documentation package.
